FROM ubuntu:20.04

USER root

RUN apt-get update && \
  DEBIAN_FRONTEND="noninteractive" \
  apt-get install -y --no-install-recommends \
    python3.9 \
    gitlab-runner \
    python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ARG CWD=/usr/src/apps/pact_consumer/

WORKDIR ${CWD}
COPY . ${CWD}
RUN chmod u+x ./scripts/*

RUN groupadd --gid 999 appuser && \
    useradd \
      --system \
      --create-home \
      --uid 999 \
      --gid appuser \
      appuser && \
    chown  -R appuser /usr/src/apps/pact_consumer/

USER appuser

RUN pip3 install --user -r requirements.txt

ENV PYTHONPATH=${CWD}
