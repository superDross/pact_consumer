import os
from unittest import mock

import pytest
from hello import say_hello
from pact import Consumer, Provider

PACT_MOCK_HOST = "localhost"
PACT_MOCK_PORT = 1234

PACT_BROKER_URL = os.environ.get("PACT_BROKER_URL", "http://pact-broker:9292")
PACT_BROKER_USERNAME = "broker"
PACT_BROKER_PASSWORD = "broker"

# define where the pact files will live: /consumer/tests/pacts/
THIS_DIR = os.path.dirname(os.path.realpath(__file__))
PACT_DIR = os.path.join(THIS_DIR, "pacts")
LOG_DIR = os.path.join(THIS_DIR, "logs")

VERSION = 1


@pytest.mark.parametrize(
    "forename,fullname",
    [
        ("david", "David McDanger"),
        ("ben", "Ben McDanger"),
        ("xavier", "Xavier Guy"),
        ("John", "John Shufflebottom"),
    ],
)
def test_using_mocks(forename, fullname):
    """
    Test using traditional mocking.
    """
    with mock.patch("requests.get") as mocked_get:
        mocked_resp = mock.Mock()
        mocked_get.return_value = mocked_resp
        mocked_resp.json.return_value = {"fullname": fullname}
        msg = say_hello(forename, "http://fake-site.net")
    assert msg == f"Hello. My name is {fullname}."


@pytest.fixture(scope="session")
def pact(request):
    """
    Creates a pact json file in the defined directory and uploads it
    to the given broker url.
    """
    consumer = Consumer("FullNameClient", version=VERSION)
    pact = consumer.has_pact_with(
        Provider("FullName"),
        host_name=PACT_MOCK_HOST,
        port=PACT_MOCK_PORT,
        log_dir=LOG_DIR,
        pact_dir=PACT_DIR,
        publish_to_broker=True,
        broker_base_url=PACT_BROKER_URL,
        broker_username=PACT_BROKER_USERNAME,
        broker_password=PACT_BROKER_PASSWORD,
    )
    pact.start_service()
    # atexit.register(pact.stop_service)
    yield pact
    pact.stop_service()


@pytest.mark.parametrize(
    "forename,fullname",
    [
        ("david", "David McDanger"),
        ("ben", "Ben McDanger"),
        ("xavier", "Xavier Guy"),
        ("John", "John Shufflebottom"),
    ],
)
def test_using_pact(forename, fullname, pact):
    """
    Instead of mocking objects we configure the pact mock provider to
    to respond with a given set of data.
    """
    (
        pact.given(f"A forename parameter begining with the letter {forename[0]}")
        .upon_receiving(f"A request with the forename parameter {forename}")
        .with_request("GET", "/create", query={"forename": forename})
        .will_respond_with(200, body={"fullname": fullname})
    )
    with pact:
        expected = f"Hello. My name is {fullname}."
        assert say_hello(forename, pact.uri) == expected
        pact.verify()
