import sys

import requests


def say_hello(forename: str, base_url: str) -> str:
    resp = requests.get(f"{base_url}/create?forename={forename}")
    resp.raise_for_status()
    fullname = resp.json().get("fullname")
    return f"Hello. My name is {fullname}."


if __name__ == "__main__":
    BASE_URL = "http://127.0.0.1:8067"
    msg = say_hello(sys.argv[1], BASE_URL)
    print(msg)
