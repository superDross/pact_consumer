TOKEN = $(ls)

_remove_old_builds:
	rm -rf builds/

build:           ## build container
	docker-compose -f docker-compose.yml build ${BUILD_ARGS}

build-no-cache:  ## build docker development image from scratch
	BUILD_ARGS=--no-cache $(MAKE) build

pull-broker:
	docker-compose -p pact-broker -f docker-compose.yml pull

up-broker:       ## spin up the pact broker container
	$(MAKE) pull-broker
	docker-compose -f docker-compose.yml up -d pact-broker

down:
	docker-compose -f docker-compose.yml down

test:            ## test consumer code and upload pact to broker
	$(MAKE) _remove_old_builds
	$(MAKE) up-broker
	docker-compose -f docker-compose.yml run pact_consumer python3 -m pytest

register-runner: ## register gitlab runner with a given token
	@echo make sure you have gitlab-runner installed locally and a runner token in ./.token file
	sleep 1
	./scripts/register-runner.sh

simulate-ci:     ## use gitlab-runner to execute the gitlab ci locally
	$(MAKE) _remove_old_builds
	$(MAKE) up-broker
	gitlab-runner exec shell test
	gitlab-runner exec shell verify
	gitlab-runner exec shell can-i-deploy
	gitlab-runner exec shell deploy

help:            ## show this message
	@echo
	@echo Targets:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/##//' -e 's/^/  /' -e 's/://'
	@echo
